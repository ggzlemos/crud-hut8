const mongoose = require('../database');

const PostChema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },

    title:{
        type: String,
        required: true,
    },

    likes:{
        type: Number,
        default: 0,
    },

    author:{
        type: String,
        required: true,
    },

    body:{
        type: String,
        default: 'Nothing has been writen'
    }
});

//definindo o Post
const Post = mongoose.model('Post', PostChema);

//exportando esse Post
module.exports = Post;
