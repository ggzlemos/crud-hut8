//ARQUIVO PARA CRIAR A CONEXÃO COM O BANCO DE DADOS
const mongoose = require('mongoose');
require('dotenv').config();
require('../index');

const MongoClient = require('mongodb').MongoClient;
const uri = process.env.BD_URL; 
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology:true });
client.connect(err => {
  const collection = client.db("test").collection("devices");
  // perform actions on the collection object
  client.close();
});

mongoose.connect(process.env.BD_URL, { useNewUrlParser: true, useUnifiedTopology:true });

//indicando qual a classe de Promisses que o mongoose vai utilizar
//Esse procedimento é padrão para os projetos que vou criar no futuro
mongoose.Promise = global.Promise;

module.exports = mongoose;

