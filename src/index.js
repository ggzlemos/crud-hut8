const express = require('express');
const bodyParser = require('body-parser');





const app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}));

//req = parâmetros da requisção. Ex.: token de autenticação, parâmetros, etc
//res = objeto de resposta ao usuário
/* app.get('/', (req, res) =>{
    res.send('OK!');
}) */

//repassando o app para o controller
require('./controllers/PostController')(app);


app.listen(process.env.PORT|| 3000);