const express = require('express');

const bodyParser = require('body-parser');

const Post = require('../models/Post');
const router = express.Router();

//router.use(authMiddleware);

//aqui, vou definir as rotas para o Post
router.post('/register', async (req, res) => {
    try {
        const post = await Post.create(req.body);

        return res.send({ post });
    }
    catch (err) {
        return res.status(400).send({ error: 'Registration failed' });
    }
});

router.get('/', async (req, res) => {
    try {
        const get = await Post.find();
        return res.send({ get });
        //return get;
    } catch (error) {
        return res.status(400).send({ error: 'Erro' });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const gege = await Post.findById(req.params.id );
        return res.send({ gege });
    } catch (error) {
        return res.status(400).send({ error: 'Post nao existe' });
    }
});

router.put('/:id', async (req, res) => {
    try {
        const gege = await Post.findByIdAndUpdate(req.params.id, req.body);
        return res.send("Atualizado com sucesso");
    } catch (error) {
        return res.status(400).send({ error: 'Post nao existe' });
    }
});

router.delete('/:id', async (req, res) => {

    try {
        await Post.findByIdAndRemove(req.params.id);
        return res.send("Excluído com sucesso");
    } catch (error) {
        return res.status(400).send({ error: 'Post nao existe' });
    }

});

//referenciando o app
module.exports = app => app.use('/post', router);
module.exports = app => app.use('/', router);
